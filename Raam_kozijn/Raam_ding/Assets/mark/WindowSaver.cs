﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
 * todo
 * get current window prefab
 * duplicate prefab and spawn it somewere els
 * reset input values
 * ???
 * profit
 * */
public class WindowSaver : MonoBehaviour
{

    private GameObject WindowPrefab;
    private int NumberOfSavedWindows = 0;
    [SerializeField] private WindowMaker maker;
    [SerializeField] private InputManager im;
    [SerializeField] private Transform[] SpawnPos;
    public void SaveWindow()
    {
        if (NumberOfSavedWindows >= SpawnPos.Length)
        {
            Debug.LogError("Window limit reached!");
            return;
        }

        WindowPrefab = maker.instantiadedGM;
        WindowPrefab.transform.position = new Vector3(0, 0, 0);
        GameObject go = Instantiate(WindowPrefab, SpawnPos[NumberOfSavedWindows].position, new Quaternion(-0, 0, 0, 0));
        go.transform.Rotate(Vector3.left, 90, Space.World);
        NumberOfSavedWindows++;
        im.inputFieldHeigth.text = "";
        im.inputFieldLenth.text = "";
        maker.CreateNewWindow();

    }
}
