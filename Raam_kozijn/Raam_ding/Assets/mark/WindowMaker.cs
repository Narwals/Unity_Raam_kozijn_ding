﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindowMaker : MonoBehaviour
{
    [SerializeField]
    private GameObject windowPrefab;
    public GameObject instantiadedGM;
    private GameObject balk1;
    [SerializeField]
    private InputManager im;
    [SerializeField]
    void Start()
    {
        CreateNewWindow();
    }

    void Update()
    {
        float x = im.calc.X();
        float y = im.calc.Y();
   
        instantiadedGM.transform.localScale = new Vector3(x,.2f,y);
  
    }
    public GameObject GetInstantiatedGameObjects()
    {
        if(instantiadedGM != null)
        {

        return instantiadedGM;
        }
        else
        {
            return null;
        }
    }
    public void CreateNewWindow()
    {
        instantiadedGM = Instantiate(windowPrefab, transform);

    }
}
