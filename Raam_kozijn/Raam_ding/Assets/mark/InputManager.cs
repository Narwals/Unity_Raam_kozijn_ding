﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputManager : MonoBehaviour
{
    [SerializeField]
    public InputField inputFieldLenth;
    [SerializeField]
    public InputField inputFieldHeigth;
    [SerializeField]
    private Dropdown dropDownGlassType;
    [SerializeField]
    private Dropdown dropDownKozijnType;
    [SerializeField]
    private Text outputText;
    public Calculator calc = new Calculator();
    void Start()
    {

    }

    void Update()
    {
        outputText.text = "Kosten = € "+ calc.Calculate(inputFieldLenth.text,inputFieldHeigth.text,dropDownGlassType,dropDownKozijnType).ToString();
    }
}
