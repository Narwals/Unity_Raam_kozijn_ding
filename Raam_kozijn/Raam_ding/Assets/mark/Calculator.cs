﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
public class Calculator
{

    float price;
    float priceKozijn;

    float x;
    float y;


    private float CalculateGlass(float lente, float breete, Dropdown glassType)
    {
        if (glassType.value == 0)
        {
            price = 30;
        }
        else if (glassType.value == 1)
        {
            price = 45;

        }
        else if (glassType.value == 2)
        {
            price = 50;
        }
        else if (glassType.value == 3)
        {
            price = 55;
        }
        return (lente * breete) * price;
    }

    private float CalculateKozijn(float lente, float breete, Dropdown kozijnType)
    {
        if (kozijnType.value == 0)
        {
            priceKozijn = 3;
        }
        else if (kozijnType.value == 1)
        {
            priceKozijn = 7;

        }
        else if (kozijnType.value == 2)
        {
            priceKozijn = 12;
        }
    
           
        return  (4*(lente * breete)) * priceKozijn;
    }
    public float Calculate(string lenthe,string breete,Dropdown glassType ,Dropdown kozijnType)
    {
       
        float.TryParse(lenthe, out x);
        float.TryParse(breete, out y);
      
        return CalculateGlass(x,y,glassType) + CalculateKozijn(x,y,kozijnType);
    }
    public float X()
    {
        return x;
    }
    public float Y()
    {
        return y;
    }
}
