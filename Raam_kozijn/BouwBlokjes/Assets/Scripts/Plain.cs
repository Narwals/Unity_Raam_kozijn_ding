﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Plain : MonoBehaviour
{


    [SerializeField]
    private GameObject cube;
    [SerializeField]
    private Slider m_slider;
    private List<GameObject> m_cube = new List<GameObject>(); 

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        m_slider.onValueChanged.AddListener(delegate { valueChange(); });

    }
    public void valueChange()
    {
        foreach (GameObject item in m_cube)
        {
            Destroy(item);
        }
        for (int y = 0; y < m_slider.value; y++)
        {
            for (int x = 0; x < m_slider.value; x++)
            {
                m_cube.Add(Instantiate(cube, new Vector3(x, y, 0), Quaternion.identity));
            }
        }
    }

}
